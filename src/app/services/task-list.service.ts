import { Injectable } from '@angular/core'

@Injectable()
export class TaskListService {
  static taskLists = [];

  // Read localstorage to get available task lists
  constructor() {
    for(var i=0, len=localStorage.length; i<len; i++) {
      var key = localStorage.key(i);
      var value = localStorage[key];
      TaskListService.taskLists.push(value);
    }
  }

  // Return available task lists from localstorage
  getCurrentTaskLists() {
    return TaskListService.taskLists;
  }

  addTaskLists(task) {
    TaskListService.taskLists.push(task);
  }
}