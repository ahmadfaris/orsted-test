import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TaskListService } from '../services/task-list.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['../app.component.scss']
})
export class InputComponent implements OnInit {
  newTask: string = ""
  addMessage: string = ""
  showMessage: boolean = false

  constructor(public taskListService: TaskListService) { }

  ngOnInit() {
  }
  
  onAddTask(addTaskForm: NgForm) {
    localStorage.setItem('tasks + ' + addTaskForm.value.newTask, addTaskForm.value.newTask)
    this.taskListService.addTaskLists(addTaskForm.value.newTask);
    this.addMessage = "Successfully add " + addTaskForm.value.newTask + " task to the list"
    this.showMessage = true
    addTaskForm.resetForm()
  }
}
