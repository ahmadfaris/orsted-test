import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { TaskListService } from '../services/task-list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['../app.component.scss']
})
export class ListComponent implements OnInit {
  tasks: Array<any> = []
  pageMessage: string = ""
  removeMessage: string = ""
  showMessage: boolean = false

  constructor(public taskListService: TaskListService) { }

  ngOnInit() {
    this.getTaskList()
  }
  
  getTaskList() {
    this.pageMessage = ""
    this.tasks = this.taskListService.getCurrentTaskLists();
    (this.tasks.length <= 0) && (this.pageMessage = "Task list is empty")
    
  }

  removeTaskList(task) {
    localStorage.removeItem('tasks + ' + task)
    let index = this.tasks.findIndex(arrTask => arrTask == task)
    this.tasks = this.tasks.splice(index, 1)
    this.removeMessage = "Successfully remove " + task + " task from the list"
    this.showMessage = true
    this.getTaskList()
  }
}
